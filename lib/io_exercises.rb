# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.

def guessing_game

answer = rand(100) +1
num_guesses = 0
guess = 0

  until won?(guess, answer)
    num_guesses +=1
    puts "Guess a number 1-100!"
    guess = gets.chomp.to_i
    if guess > answer
      puts "#{guess} is too high"
    elsif guess < answer
      puts "#{guess} is too low"
    else
      puts "congrats! Your guess of #{guess} was perfect.  You won in #{num_guesses} guesses."
    end
  end
end

def won?(guess, answer)
  if guess == answer
    return true
  else
    return false
  end
end

def file_shuffler
  puts "what file do you want to shuffle?"
  file_in = gets.chomp
  unless File.file?(file_in)
    puts "that file does not exist!  Try again"
    file_shuffler
  end
  lines = File.readlines(file_in)

  lines_shuffled = lines.shuffle
  file_in_no_ext = file_in[0..-5]

  shuffled_file = File.open("#{file_in_no_ext}-shuffled.txt", "w")
  shuffled_file.puts File.readlines(file_in).shuffle
  shuffled_file.close
end

#guessing_game
file_shuffler
